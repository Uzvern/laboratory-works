### Installation
- `npm i`
- `yarn serve`
- go to `localhost:8080`

### Задание
Vue приложение показывает перечень документов и сам документ

- конфигурационный файл с перечнем документов и где брать их содержимое для них лежит отдельно от приложения
- приложение загружает этот перечень и выводит на экран
- при клике на документ выводим его содержимое и можем вернуться к списку документов

дополнения:

- в идеале документ может содержать html верстку изображения, все это должно лежать отдельно от самого приложения
- в идеале выводить содержимое документов сверстанным модальным окном со скроллом и крестиком чтоб закрыть его

Для понимания: 

Главный экран - это перечень лабораторных работ.
При клике на конкретную лабораторную работу открываются методические материалы этой работы.

### What we will use?

[Vue](https://vuejs.org/)  
[Vue-router](https://router.vuejs.org/)  
[Vuex](https://vuex.vuejs.org/)  
[Axios](https://github.com/axios/axios)  
[Bootstrap](https://getbootstrap.com/docs/3.3/getting-started/)  

### Данные:
Данные будем получать в `JSON` при помощи axios

### Форматы данных:
#### Конфигурационный файл с перечнем документов и где брать их содержимое для них  

```JSON
{
  "response": {
    "items": [
      {
        "type": "link",
        "id": 1,
        "attributes": {
          "url": "https://api.myjson.com/bins/so0be"
        },
        "relationships": {
          "work": {
            "title": "Modal with text",
            "visible": true
          }
        }
      },
      {
        "type": "link",
        "id": 2,
        "attributes": {
          "url": "https://api.myjson.com/bins/geh96"
        },
        "relationships": {
          "work": {
            "title": "Modal With HTML",
            "visible": true
          }
        }
      }
    ]
  }
}
```

#### Содержимое документа с текстом

```JSON
{
  "response": {
    "type": "work",
    "id": 1,
    "attributes": {
      "datatype": "text",
      "text": "Первые впечатления\n\n\nПереночевав в гостинице в Гуаякиле, мы сели к агенту в машину и поехали на судно в Пуэрто Боливар. Доехали вопреки ожиданиям быстро, примерно за 3-4 часа. Погода была пасмурная и даже не смотря на то, что мы находимся недалеко от экватора, было прохладно. Почти все время, пока мы ехали, по обе стороны дороги были банановые плантации, но все равно в голове не укладывается: эти бананы грузят на суда в нескольких портах Эквадора десятками тысяч тонн каждый день, круглый год. Это ж несчастные бананы должны расти быстрее чем грибы.\n\n\nДороги.\nДороги в Эквадоре практически идеальные, хотя населенные пункты выглядят очень бедно. На дорогах много интересных машин, например очень много грузовиков - древних Фордов, которые я никогда раньше не видел. А еще несколько раз на глаза попадались старенькие Жигули :) А еще если кого-то обгоняешь и есть встречная машина, она обязательно включает фары. На больших машинах - грузовиках и автобусах, обязательно красуется местный тюнинг: машины разукрашенные, либо в наклейках, и обязательно везде огромное множество светодиодов, как будто новогодние елки едут и переливаются всеми цветами.\n\n\nСудно.\nНа первый взгляд судно неплохое, в относительно хорошем состоянии, хотя и 92 года постройки. Экипаж 19 человек - 11 русских и 8 филиппинцев, включая повара. Говорят, периодически становится тоскливо от егошних кулинарных изысков. Филиппинцы здесь рядовой состав, за ними постоянно нужно следить чтобы не натворили чего, среди них только один матрос по-настоящему ответственный и с руками из нужного места, все понимает с полуслова. Остальные - типичные Равшаны да Джамшуты. А еще один из них - гомосек О___о, в добавок к этому он опасный человек, в том плане, что легко впадает в состояние ступора и отключает мозг: был случай как он закрыл одного матроса в трюме, тот орал и тарабанил внутри, это заметил боцман, начал орать на этого персонажа, который, в свою очередь испуганно выпучив глаза, трясущимися руками продолжал закручивать барашки. В итоге боцман его отодвинул и выпустил матроса из трюма. Общение на английском языке, но из-за акцента не всегда с первого раз понятно что филиппинцы говорят, особенно по рации. Напимер, говорит он тебе: Бикарпуль! Бикарпуль! А потом, когда уже поздно, выясняется что это было \"Be careful!\"\n\nРабота.\nСразу, как только мы заселились, я не успел разложить вещи, как в мою голову ворвался такой поток информации, что ни в сказке сказать, ни топором не вырубить. Во-первых, на судне абсолютно все бумаги - мануалы, журналы, и так далее - все на английском языке. Даже блокнотик, в который записываются отчеты по грузовым операциям - и тот на английском. Бумаги... ооооо... Их тысячи, лежат в сотнях папок, плюс огромное количество документов на компьютерах. Это мне просто разорвало мозг в клочья, потому что с этим объемом информации надо ознакомиться и научиться работать в кротчайшие сроки. Постоянная беготня, постоянная суета, совсем не легко. А также надо как можно быстрее разобраться со всем оборудованием на мостике, а там его мама не горюй. В общем, пока что, свободного времени нет вообще. Абсолютно. Только ночью с 00:00 до 06:00 можно поспать. Но это продлится не долго, буквально 1-2 недели, потом океанский переход до Европы, можно будет уже спокойно стоять вахты, а в свободное время читать книги компании Seatrade, на случай если в Европе придет проверка и будет задавать вопросы.\n\nНу и немного о приятном.\nНеплохая одноместная каюта. Внутри несколько шкафов и полок, удобная кровать, койка напередохнуть, стол, стул, умывальник и внутрисудовой телефон. Также выдали 2 белых офицерских комбинезона с символикой компании, каску и персональную рацию. В моем распоряжении офицерский душ со стиральной машинкой и офицерская столовая. Во время швартовых операций мое место на мостике. Хотя и матросы на палубе не сильно напрягаются - там установлены гидравлические лебедки, не надо швартовные концы тягать вручную.\n\nНа этом пока все, фоток пока что нет, потому что просто некогда этим заниматься. Даже этот текст я пишу потому что просто появилось свободных 2 часа, отпустили отдохнуть, так как впереди 6-часовая вахта, а сразу после нее отшвартовка и идем в Гуаякиль, а во время отшвартовки и пока лоцман на судне - мое место на мостике. Так что, предстоит основательно заколебаться.\n\n8 августа 2013г. Пуэрто Боливар, Эквадор."
    }
  }
}
```

#### Содержимое документа с версткой

```JSON
{
  "response": {
    "type": "work",
    "id": 2,
    "attributes": {
      "datatype": "html",
      "text": "<form><div class=\"form-group\"><label for=\"exampleInputEmail1\">Email address</label><input type=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" placeholder=\"Email\"></div><div class=\"form-group\"><label for=\"exampleInputPassword1\">Password</label><input type=\"password\" lass=\"form-control\" id=\"exampleInputPassword1\" placeholder=\"Password\"></div><div class=\"form-group\">    <label for=\"exampleInputFile\">File input</label>    <input type=\"file\" id=\"exampleInputFile\">    <p class=\"help-block\">Example block-level help text here.</p>  </div>  <div class=\"checkbox\">    <label>      <input type=\"checkbox\"> Check me out    </label>  </div>  <button type=\"submit\" class=\"btn btn-default\">Submit</button></form>"
    }
  }
}
```