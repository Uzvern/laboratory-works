/**
 * declOfNum(['промо-код','промо-кода','промо-кодов'], 1) - промо-код
 * declOfNum(['промо-код','промо-кода','промо-кодов'], 2) - промо-кода
 * declOfNum(['промо-код','промо-кода','промо-кодов'], 5) - промо-кодов
 *
 * @return     {string}  { text of count }
 */
export const declOfNum = (function(){
    let cases = [2, 0, 1, 1, 1, 2];
    let declOfNumSubFunction = function(titles, number){
        number = Math.abs(number);
        return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
    }
    return function(_titles) {
        if ( arguments.length === 1 ){
            return function(_number){
                return declOfNumSubFunction(_titles, _number)
            }
        }else{
            return declOfNumSubFunction.apply(null,arguments)
        }
    }
})()

/**
 * Возвращает или устанавливает значение для "глубокого" объекта
 *
 * @param      {<type>}             obj       The object
 * @param      {string || array}    path      The path
 * @param      {<type>}             value     The value
 */
export const updatePath = function( obj, path, value ) {
    let parts = (typeof path === 'string') ? path.split('.') : path

    let tmp
    for( let i = 0; i < parts.length; i++ ) {
        tmp = obj[ parts[ i ] ]
        if( value !== undefined && i == parts.length - 1 ) {
            tmp = obj[ parts[ i ] ] = value
        }
        else if( tmp === undefined ) {
            tmp = obj[ parts[ i ] ] = {}
        }
        obj = tmp
    }
    return obj
}

export const setDocumentTitle = ( title ) => {
    document.title = title
}

export const stripScripts = function( html ) {
    var div = document.createElement( 'div' );
    div.innerHTML = html;
    var scripts = div.getElementsByTagName( 'script' );
    var i = scripts.length;
    while (i--) {
      scripts[i].parentNode.removeChild( scripts[i] );
    }
    return div.innerHTML;
  }