import Vue from 'vue'
import Vuex from 'vuex'

import * as CON from '../constants'
import { updatePath }from '../utils/utils'
import * as axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {

  },
  state: {
    laboratory: {
      links: {
        status: CON.INITIAL,
        entities: []
      },
      works: {
        status: CON.INITIAL,
        entities: []
      }
    }
  },
  getters: {
    getAllState( state ) {
      return state
    },
    getAllLinks( state ) {
      return state.laboratory.links
    },
    getAllWorks( state ) {
      return state.laboratory.works
    }
  },
  mutations: {
    setState( state, { ctx, value } ) {
      // from utils
      updatePath( state, ctx, value )
    }
  },
  actions: {
    getLinksForWorks( { commit } ) {
      commit('setState', {
        ctx: ['laboratory', 'links', 'status'],
        value: CON.PENDING
      })

      return axios.get(CON.URL_FOR_LINKS_WITH_WORKS , {
        timeout: 5000
      })
        // проверяем статус ответа и наличие тела ответа
        .then(res => {
          if (res.status === 200 && res.data.response) {
            commit('setState', {
              ctx: ['laboratory', 'links', 'entities'],
              value: res.data.response.items
            })

            commit('setState', {
              ctx: ['laboratory', 'links', 'status'],
              value: CON.COMPLETED
            })

            return res
          }

          console.warn( res,status, res.statusText )
          commit('setState', {
            ctx: ['laboratory', 'links', 'status'],
            value: CON.FAILED
          })
        })
        .catch(err => {
          commit('setState', {
            ctx: ['laboratory', 'links', 'status'],
            value: CON.FAILED
          })

          console.warn( err )
        })
    },
    getWork( { state, commit }, { url, workId } ) {
      // Не загружаем по-новой, если уже загружено
      let currentWork = state.laboratory.works.entities.filter(el => el.id === workId)
      if ( currentWork.length ) return currentWork

      commit('setState', {
        ctx: ['laboratory', 'works', 'status'],
        value: CON.PENDING
      })

      return axios.get(url, {
        timeout: 5000
      })
      .then( res => {
        if (res.status === 200 && res.data.response) {
          // Добавляем загруженные данные к остальным
          commit('setState', {
            ctx: ['laboratory', 'works', 'entities'],
            value: [...state.laboratory.works.entities, res.data.response]
          })

          commit('setState', {
            ctx: ['laboratory', 'works', 'status'],
            value: CON.COMPLETED
          })

          return res
        }

        commit('setState', {
          ctx: ['laboratory', 'works', 'status'],
          value: CON.FAILED
        })
      })
      .catch(err => {
        commit('setState', {
          ctx: ['laboratory', 'works', 'status'],
          value: CON.FAILED
        })

        console.warn( err )
      })
    }
  },
  plugins: [

  ]
})
