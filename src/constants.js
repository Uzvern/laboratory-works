export const INITIAL = {
    initial: true,
    pending: false,
    completed: false,
    failed: false,
}

export const PENDING = {
    initial: false,
    pending: true,
    completed: false,
    failed: false,
}

export const COMPLETED = {
    initial: false,
    pending: false,
    completed: true,
    failed: false,
}

export const FAILED = {
    initial: false,
    pending: false,
    completed: false,
    failed: true,
}

// ссылка на JSON со списком ссылок на все работы
export const URL_FOR_LINKS_WITH_WORKS = 'https://api.myjson.com/bins/hlcgq'
